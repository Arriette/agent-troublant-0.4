import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

try {
  admin.initializeApp();
} catch (e) {}

/**
 * Update database when profil has been modified.
 * TODO: Create a log for refund
 * TODO: Create hitory profil (dept since the beginning, current dept, name, path, image) (for the list of history)
 * TODO: update the history money win for Agent
 * TODO: Not ok with performance, in edit case
 */
export const updateProfile = functions.firestore
  .document("profiles/{profileID}")
  .onUpdate((change, context) => {
    // Get old and new data.
    const newValue = change.after.data();
    const previousValue = change.before.data();
    const date = new Date();
    admin
      .firestore()
      .collection("cards")
      .doc(`Card_${newValue.path}`)
      .set(
        {
          description: newValue.description || "",
          timestamp: Date.now(),
          name: newValue.name,
          type: "profile",
          profile_path: previousValue.path
        },
        { merge: true }
      )
      .catch(err => console.log(err));

    /**
     * Case of refund
     */
    if (previousValue.wallet !== 0 && newValue.wallet === 0) {
      const soldGiven = Math.round(previousValue.wallet*100)/100

      /**
       * Create a refund profile log.
       */
      return admin
        .firestore()
        .collection("logs")
        .add({
          soldGiven: soldGiven,
          typeLog: `refund`,
          date:
            date.getDate() +
            "/" +
            ("0" + (date.getMonth() + 1)).slice(-2) +
            "/" +
            date.getFullYear() +
            "  " +
            ("0" + (date.getHours() + 2)).slice(-2) +
            ":" +
            ("0" + date.getMinutes()).slice(-2),
          timestamp: Date.now(),
          name: newValue.name,
          typeElement: "refund",
          path: newValue.path,
          fullSold: previousValue.wallet
        })
        .then(() => {
          admin
            .firestore()
            .collection("history")
            .doc(`Dept_${newValue.path}`)
            .get()
            .then(function(doc) {
              // Does the group exist?
              if (doc.exists) {
                const historyDept = doc.data();
                admin
                  .firestore()
                  .collection("history")
                  .doc(`Dept_${newValue.path}`)
                  .set(
                    {
                      name: newValue.name,
                      path: newValue.path,
                      currentDept: 0,
                      fullSoldSinceBegining: 
                        historyDept.fullSoldSinceBegining + soldGiven

                    },
                    { merge: true }
                  )
                  .catch(err => console.log(err.message));
              }
            })
            .catch(err => console.log(err.message));
        })
        .catch(err => console.log(err.message));
    } else {
      admin
        .firestore()
        .collection("history")
        .doc(`Dept_${newValue.path}`)
        .get()
        .then(function(doc) {
          // Does the profil exist?
          if (doc.exists) {
            admin
              .firestore()
              .collection("history")
              .doc(`Dept_${newValue.path}`)
              .set(
                {
                  name: newValue.name,
                  path: newValue.path
                },
                { merge: true }
              )
              .catch(function(error) {
                console.error(error);
              });
          }
        })
        .catch(err => console.log(err));
    }
    return null;
  });

/**
 * Update database when product has been modified.
 */
export const updateProduct = functions.firestore
  .document("products/{productID}")
  .onUpdate((change, context) => {
    // Get old and new data.
    const newValue = change.after.data();
    const previousValue = change.before.data();
    const date = new Date();

    /**
     * Update the cards
     * No matter the action, we update the card.
     * TODO: When we just update the wallet we don't have to run this.
     */
    admin
      .firestore()
      .collection("cards")
      .doc(`Card_${newValue.path}`)
      .set(
        {
          description: newValue.description || "",
          timestamp: Date.now(),
          name: newValue.name,
          author: newValue.author,
          image: newValue.image,
          price: newValue.price,
          stock: newValue.stock,
          type: "product",
          product_path: newValue.path
        },
        { merge: true }
      )
      .catch(err => console.log(err.message));

    /**
     * Case of reception
     */
    if (previousValue.stock < newValue.stock) {
      const unities = newValue.stock - previousValue.stock; /*Unities recieve*/

      /**
       * Create a recieve product log.
       */
      return admin
        .firestore()
        .collection("logs")
        .add({
          unities: unities,
          typeLog: `recieve`,
          date:
            date.getDate() +
            "/" +
            ("0" + (date.getMonth() + 1)).slice(-2) +
            "/" +
            date.getFullYear() +
            "  " +
            ("0" + (date.getHours() + 2)).slice(-2) +
            ":" +
            ("0" + date.getMinutes()).slice(-2),
          timestamp: Date.now(),
          name: newValue.name,
          typeElement: "product",
          path: newValue.path,
          priceSold: 0
        })
        .catch(err => console.log(err.message));

      /**
       * Case of sell
       */
    } else if (previousValue.stock > newValue.stock) {
      const unities = previousValue.stock - newValue.stock; /*Unities sold*/

      /**
       * Case of sell with free Price
       */
      if (Number(newValue.price) === 0) {

        /**
         * Update the wallet of the profile.
         */
        // Get the wallet
        admin
          .firestore()
          .collection("profiles")
          .doc(newValue.path_author)
          .get()
          .then(function(doc) {
            //The profile exists?
            if (doc.exists) {
              const author = doc.data();
              //Update the wallet
              admin
                .firestore()
                .collection("profiles")
                .doc(newValue.path_author)
                .set(
                  {
                    wallet: author.wallet + newValue.priceSold * unities
                  },
                  { merge: true }
                )
                .then(() => {
                  admin
                    .firestore()
                    .collection("history")
                    .doc(`Dept_${author.path}`)
                    .set(
                      {
                        currentDept:
                          author.wallet + newValue.priceSold * unities
                      },
                      { merge: true }
                    )
                    .catch(err => console.log(err.message));
                })
                .catch(err => console.log(err.message));
            }
          })
          .catch(err => console.log(err.message));

        /**
         * Create the log to confirm free price sell.
         */
        return admin
          .firestore()
          .collection("logs")
          .add({
            unities: unities,
            price: 0,
            typeLog: `sell`,
            date:
              date.getDate() +
              "/" +
              ("0" + (date.getMonth() + 1)).slice(-2) +
              "/" +
              date.getFullYear() +
              "  " +
              ("0" + (date.getHours() + 2)).slice(-2) +
              ":" +
              ("0" + date.getMinutes()).slice(-2),
            timestamp: Date.now(),
            name: newValue.name,
            typeElement: "product",
            path: newValue.path,
            priceSold: newValue.priceSold
          })
          .catch(err => console.log(err.message));
      } else if (Number(newValue.priceSold) === 0) {
        /**
         * Case of sell with fix Price
         */

        /**
         * Update the wallet of the profile.
         */
        // Get the wallet
        admin
          .firestore()
          .collection("profiles")
          .doc(newValue.path_author)
          .get()
          .then(function(doc) {
            // Does the author exists?
            if (doc.exists) {
              const author = doc.data();
              // Update the wallet
              admin
                .firestore()
                .collection("profiles")
                .doc(newValue.path_author)
                .set(
                  {
                    wallet: author.wallet + newValue.price * 0.7 * unities
                  },
                  { merge: true }
                )
                .then(() => {
                  admin
                    .firestore()
                    .collection("history")
                    .doc(`Dept_${author.path}`)
                    .set(
                      {
                        currentDept:
                          author.wallet + newValue.price * 0.7 * unities
                      },
                      { merge: true }
                    )
                    .catch(err => console.log(err.message));
                })
                .catch(err => console.log(err.message));
            }
          })
          .catch(err => console.log(err.message));

        /**
         * Add commission to the reciepe of the month.
         */
        admin
          .firestore()
          .collection("reciepes")
          .doc(`${new Date().toJSON()}`)
          .set({
            author: newValue.path_author,
            product: newValue.path,
            reciepe: newValue.price * 0.3 * unities,
            month: new Date().getMonth(),
            year:new Date().getFullYear(),
            date:
            date.getDate() +
            "/" +
            ("0" + (date.getMonth() + 1)).slice(-2) +
            "/" +
            date.getFullYear() +
            "  " +
            ("0" + (date.getHours() + 2)).slice(-2) +
            ":" +
            ("0" + date.getMinutes()).slice(-2),
          timestamp: Date.now()
          })
          .catch(err => console.log(err.message));
          
        /**
         * Create the log to confirm fix price sell.
         */
        return admin
          .firestore()
          .collection("logs")
          .add({
            unities: unities,
            price: newValue.price,
            typeLog: `sell`,
            date:
              date.getDate() +
              "/" +
              ("0" + (date.getMonth() + 1)).slice(-2) +
              "/" +
              date.getFullYear() +
              "  " +
              ("0" + (date.getHours() + 2)).slice(-2) +
              ":" +
              ("0" + date.getMinutes()).slice(-2),
            timestamp: Date.now(),
            name: newValue.name,
            typeElement: "product",
            path: newValue.path,
            priceSold: 0
          })
          .catch(err => console.log(err.message));
      }
    }
    return null;
  });
