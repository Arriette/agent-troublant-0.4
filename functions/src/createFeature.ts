import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

try {
  admin.initializeApp();
} catch (e) {}

/**
 * Create profile when user is created
 */
export const createProfile = functions.auth.user().onCreate(user => {
  admin
    .firestore()
    .collection("profiles")
    .add({
      uuid: user.uid
    })
    .catch(err => console.log(err.message));
});

/**
 * When created a profil we create a history dept document.
 */
exports.createProfileAuthor = functions.firestore
  .document("profiles/{profileId}")
  .onCreate((snap, context) => {
    const newValue = snap.data();
    const date = new Date();

    return admin
      .firestore()
      .collection("history")
      .doc(`Dept_${newValue.path}`)
      .set({
        name: newValue.name,
        fullSoldSinceBegining: 0,
        currentDept: newValue.wallet,
        path: newValue.path
      })
      .then(() => {
        admin
          .firestore()
          .collection("cards")
          .doc(`Card_${newValue.path}`)
          .set({
            description: newValue.description,
            timestamp: Date.now(),
            name: newValue.name,
            type: "profile",
            profile_path: newValue.path
          })
          .catch(err => console.log(err))
          .then(() => {
            admin
              .firestore()
              .collection("logs")
              .add({
                typeLog: `add`,
                date:
                  date.getDate() +
                  "/" +
                  ("0" + (date.getMonth() + 1)).slice(-2) +
                  "/" +
                  date.getFullYear() +
                  "  " +
                  ("0" + (date.getHours() + 2)).slice(-2) +
                  ":" +
                  ("0" + date.getMinutes()).slice(-2),
                timestamp: Date.now(),
                name: newValue.name,
                typeElement: "profile",
                path: newValue.path
              })
              .catch(err => console.log(err.message));
          })
          .catch(err => console.log(err.message));
      });
  });

/**
 * When created a product  we create card and log
 */
exports.createProduct = functions.firestore
  .document("products/{productId}")
  .onCreate((snap, context) => {
    const newValue = snap.data();
    const date = new Date();

    return admin
      .firestore()
      .collection("cards")
      .doc(`Card_${newValue.path}`)
      .set({
        description: newValue.description,
        timestamp: Date.now(),
        name: newValue.name,
        type: "product",
        image: newValue.image,
        price: newValue.price || 0,
        stock: newValue.stock,
        author: newValue.author,
        product_path: newValue.path,
        path_author: newValue.path_author
      })
      .then(() => {
        admin
          .firestore()
          .collection("logs")
          .add({
            name: newValue.name,
            typeLog: `recieve`,
            date:
              date.getDate() +
              "/" +
              ("0" + (date.getMonth() + 1)).slice(-2) +
              "/" +
              date.getFullYear() +
              "  " +
              ("0" + date.getHours()).slice(-2) +
              ":" +
              ("0" + date.getMinutes()).slice(-2),
            timestamp: Date.now(),
            typeElement: "product",
            path: newValue.path,
            unities: newValue.stock
          })
          .catch(err => console.log(err.message));
      })
      .then(() => {
        admin
          .firestore()
          .collection("logs")
          .add({
            typeLog: `add`,
            date:
              date.getDate() +
              "/" +
              ("0" + (date.getMonth() + 1)).slice(-2) +
              "/" +
              date.getFullYear() +
              "  " +
              ("0" + date.getHours()).slice(-2) +
              ":" +
              ("0" + date.getMinutes()).slice(-2),
            timestamp: Date.now(),
            name: newValue.name,
            typeElement: "product",
            path: newValue.path
          })
          .catch(err => console.log(err.message));
      })
      .catch(err => console.log(err.message));
  });
