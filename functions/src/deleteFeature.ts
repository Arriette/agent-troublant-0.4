import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

try {
  admin.initializeApp();
} catch (e) {}

/**
 * Manage deletion of product
 */
exports.deleteProduct = functions.firestore
  .document("products/{productID}")
  .onDelete((snap, context) => {
    const deletedValue = snap.data();
    const date = new Date();
    return admin
      .firestore()
      .collection("cards")
      .doc(`Card_${deletedValue.path}`)
      .delete()
      .then(() => {
        admin
          .firestore()
          .collection("logs")
          .add({
            typeLog: `delete`,
            date:
              date.getDate() +
              "/" +
              ("0" + (date.getMonth() + 1)).slice(-2) +
              "/" +
              date.getFullYear() +
              "  " +
              ("0" + date.getHours()).slice(-2) +
              ":" +
              ("0" + date.getMinutes()).slice(-2),
            timestamp: Date.now(),
            name: deletedValue.name,
            typeElement: "product",
            path: deletedValue.path
          })
          .catch(err => console.log(err.message));
      })
      .catch(err => console.log(err.message));
  });

/**
 * Manage deletion of profile
 */
export const deleteProfile = functions.firestore
  .document("profiles/{profileID}")
  .onDelete((snap, context) => {
    const date = new Date();
    const deletedValue = snap.data();
    admin
      .firestore()
      .collection("cards")
      .doc(`Card_${deletedValue.path}`)
      .delete()
      .then(() => {
        admin
          .firestore()
          .collection("logs")
          .add({
            typeLog: `delete`,
            date:
              date.getDate() +
              "/" +
              ("0" + (date.getMonth() + 1)).slice(-2) +
              "/" +
              date.getFullYear() +
              "  " +
              ("0" + date.getHours()).slice(-2) +
              ":" +
              ("0" + date.getMinutes()).slice(-2),
            timestamp: Date.now(),
            name: deletedValue.name,
            typeElement: "profile",
            path: deletedValue.path
          })
          .catch(err => console.log(err));
      })
      .catch(err => console.log(err));
  });
