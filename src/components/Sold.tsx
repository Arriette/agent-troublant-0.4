import { useContext, MouseEvent } from 'react';
import { Redirect } from 'react-router-dom';
import * as React from 'react';
// import Popup from "reactjs-popup";
import AppContext from "./AppContext";

export const SoldButton = () => {

  /**
   * TODO: Pourquoi ma redirection à la homepage ne marche pas ?
   */
  const { addNotice } = useContext(AppContext);

  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    addNotice('Vendu', 'is-success');
    return <Redirect to='/' />
  };

  /**
   * TODO: Récupérer le nom
   */
  const handleClickCancel = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    return <Redirect to='/Fanzine/Name' />
  };

  return (
    <div>
      <button onClick={handleClick}>
        <p>Confirmer</p>
      </button>
      <button onClick={handleClickCancel}>
        <p>Annuler</p>
      </button>
    </div>
  )

}

/**
 * Popine to sell products.
 * TODO : Retrieve the title and the price.
 */
const Sold = () => (
  // <Popup trigger={<button>Vente <i className="fas fa-arrow-right"></i></button>} position="right center" modal>
    // {close => (
      <div>
        <h1>Vente</h1>
        <p>Tu vends **X** exemplaire(s) de **title** à **xxx*** €.</p>
        <p>Tu confirmes ? </p>
        <a className="close" onClick={close}>
          <SoldButton />
        </a>
      </div>
    // )}
  // </Popup>
);

export default Sold;
