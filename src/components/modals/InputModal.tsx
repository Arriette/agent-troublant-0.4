import React, { useState, useContext } from "react";
import AppContext from "../AppContext";
import { updateProductStock } from "../../api/Product";



/*
 * Modal With a yes/no question
 * TODO: The notice do not appear after reload even with sleep function
 */

const InputModal = ({
  title,
  name,
  content,
  cancelText,
  confirmText,
  stock,
  unitiesFor,
  path,
  freePrice
}: any) => {

  // Main controller.oduct
  const { addNotice, resetModal } = useContext(AppContext);

  //Init the state
  const [data, setData] = useState({});

  /*
   * Process the action
   */
  function action() {
    var newStock = 0;
    // Case of a sell
    if (unitiesFor == 'sell' && stock >= data.unities) {
      var actionName = "Vente"
      newStock = Number(stock) - Number(data.unities)
      var priceSold = (data.priceSold ? data.priceSold : 0)
    }
    // Case of reception
    else if (unitiesFor == 'recieve') {
      var actionName = "Recu"
      newStock = Number(stock) + Number(data.unities)
      priceSold = 0
    }
    // Error case
    else if (stock < data.unities) {
      addNotice(`Vous ne pouvez pas vendre plus que le stock`, "is-warning");
      resetModal();
    }

    // Finally update the stock
    updateProductStock(path, newStock, priceSold)
      .then(() => {
        resetModal();
        location.reload();
      })
      .then(() => {
        addNotice(`${actionName} ${data.unities} de ${name}`, "is-success");
      })
      .catch(err => addNotice(err.message, "is-danger"));
  }

  // When the values of the form changes
  function handleChange(e: any) {
    setData({
      ...data,
      [e.target.name]: e.target.value
    });
  }

  return (
    <div className="modal is-active">
      <div className="modal-background" />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title is-size-6">{content ? content : title}</p>
          <button
            className="delete"
            aria-label="close"
            onClick={resetModal} />
        </header>
        <section className="modal-card-body">
          <div className="field">
            <div className="control">
              <input
                className="input"
                name="unities"
                type="number"
                required
                onChange={handleChange}
              />
            </div>
          </div>
          {freePrice === true &&
            <div className="field" >
              <label className="label">Prix ?</label>
              <div className="control">
                <input
                  className="input"
                  name="priceSold"
                  type="number"
                  onChange={handleChange}
                  required
                />
              </div>
            </div>
          }
        </section>
        <footer className="modal-card-foot">
          <button className="button is-danger" onClick={resetModal}>
            {cancelText}
          </button>
          <button className="button is-success" onClick={action}>
            {confirmText}
          </button>
        </footer>
      </div>
    </div>
  );
};

export default InputModal;
