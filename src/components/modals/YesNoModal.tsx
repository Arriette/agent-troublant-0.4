import React from 'react';
import AppContext from '../AppContext';

/*
* Modal With a yes/no question
*/

const YesNoModal = ({ content, cancelText, confirmText, action }: any) => {

  const { resetModal } = React.useContext(AppContext)

  return (
    <div className='modal is-active'>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-content">{content}</p>
          <button className="delete" aria-label="close" onClick={resetModal}></button>
        </header>
        <footer className="modal-card-foot">
          <button className="button is-danger" onClick={resetModal}>{cancelText}</button>
          <button className="button is-success" onClick={action}>{confirmText}</button>
        </footer>
      </div>
    </div>
  )
}

export default YesNoModal;