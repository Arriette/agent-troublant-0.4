import React, { useState, useContext } from 'react';
import { withRouter } from 'react-router';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock, faEnvelope } from '@fortawesome/free-solid-svg-icons';

// API
import { signIn } from '../api/Auth';

// Components
import AppContext from './AppContext';

// Form data interface
interface FormDataInterface {
  email: string,
  password: string,
};

// Default form values.
const defaultValues: FormDataInterface = {
  email: '',
  password: ''
};

// Login form
const LoginForm = ({ history }: any) => {

  // Form values store.
  const [formData, setFormData] = useState<FormDataInterface>(defaultValues);

  // Retrieve the add notice method from global context.
  const { addNotice } = useContext(AppContext);

  // Handle the change of the input values.
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  // Handle the submit of the form.
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    signIn(formData.email, formData.password)
      .then(() => {
        addNotice('You are now connected!', 'is-success');
        setFormData(defaultValues);
        history.push('/');
      })
      .catch((err: Error) => {
        addNotice(err.message, 'is-danger');
        setFormData(defaultValues);
      });
  }

  return (
    <>
    <form onSubmit={handleSubmit} style={{ maxWidth: '300px' }}>
      <div className="field">
        <p className="control has-icons-left has-icons-right">
          <input className="input" name="email" type="email" placeholder="Email"
            value={formData.email} onChange={handleChange} required />
          <span className="icon is-small is-left">
            <FontAwesomeIcon icon={faEnvelope} />
          </span>
        </p>
      </div>
      <div className="field">
        <p className="control has-icons-left">
          <input className="input" name="password" type="password" placeholder="Password"
            value={formData.password} onChange={handleChange} required />
          <span className="icon is-small is-left">
            <FontAwesomeIcon icon={faLock} />
          </span>
        </p>
      </div>
      <div className="field">
        <p className="control">
          <button className="button is-success">Login</button>
        </p>
      </div>
    </form>
    </>
  );
}

export default withRouter(LoginForm);
