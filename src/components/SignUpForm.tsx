import React, { useState, useContext } from 'react';

// Data
import AppContext from './AppContext';
import { signUpForm } from '../api/Auth';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from 'react-router';

//Interface
import ProfileInterface from '../api/Profile'
/**
 * Sign Up form.
 * TODO: Complete to form with Profil info
 */
const SignUpForm = ({ history }: any) => {


  // Default form values.
  let defaultValues = {
    email: '',
    password: ''
  };

   // Form values store.
   const [userData, setUserData] = useState(defaultValues);
   // Data of a profile.
  const [profileData, setProfileData] = useState<ProfileInterface | any>({});

     // Main controller.
  const { addNotice } = useContext(AppContext);

  // Handle the change of the input values.
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      try {
        if(e.target.name == "email" || "password") {
            setUserData({
                ...userData,
                [e.target.name]: e.target.value
              });
          }
          setProfileData({
              ...profileData,
              [e.target.name]: e.target.value
          })
        //Shoud I use form.reset?
      }
     catch(error){
        addNotice(error.message, 'is-danger')
     }  
  };

 /*
  * Open modal the confim the action
  * TODO: Create a confimr modal =>`L'utilisateur avec l'adresse ${form.email} a été rajouté.`
  */
  const signUp = (e: any) => {
    e.preventDefault();
    console.log(userData.email, userData.password)
    console.log(profileData)
    signUpForm(userData.email, userData.password, profileData);
  };

  return (
      <>
        <form id="signup-form" style={{ maxWidth: '300px' }} onSubmit={signUp}>
            <div className="field">
                <label className="label">Name</label>
                <div className="control">
                <input className="input" name="name" type="text" required onChange={handleChange} />
                </div>
            </div>
            <div className="field">
                <label className="label">Petite bio</label>
                <div className="control">
                    <textarea className="textarea" name="description"  onChange={handleChange}></textarea>
                </div>
            </div>
            <div className="field">
                <label className="label">Activités</label>
                <div className="control">
                    <textarea className="textarea" name="activities"  ></textarea>
                </div>
            </div>
            <div className="field">
                <label className="label">Localisation</label>
                <div className="control">
                <input className="input" name="location" type="text"  onChange={handleChange} />
                </div>
            </div>
            <div className="field">
                <label className="label">Téléphone</label>
                <div className="control">
                    <input className="input" name="phone" type="text"  onChange={handleChange} />
                </div>
            </div>
            <div className="field">
                <p className="control has-icons-left has-icons-right">
                    <input className="input" type="email" id="signup-email" name="email" placeholder="Email" onChange={handleChange} required />
                    <span className="icon is-small is-left">
                        <FontAwesomeIcon icon={faEnvelope} />
                    </span>
                </p>
            </div>
            <div className="field">
                <p className="control has-icons-left">
                    <input className="input" type="password" minLength={8} id="signup-password" name="password" placeholder="Password" onChange={handleChange} required />
                    <span className="icon is-small is-left">
                        <FontAwesomeIcon icon={faLock} />
                    </span>
                </p>
            </div>
            <button className="button is-success" >Sign up</button>
        </form> 
    </>
  );
}

export default withRouter(SignUpForm);
