import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';

//data
import './NavBar.scss';
import AppContext from './AppContext';
import { logOut } from '../api/Auth';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShieldAlt, faUserAstronaut } from '@fortawesome/free-solid-svg-icons';
import firebase from 'firebase';

// Assets
// TODO: Fix warning about unknown module
import logo from '../assets/logo.svg';

/**
 * Navigation top bar.
 * TODO: Update the style of the menu and add animation
 * TODO: Add action buttons (edit, delete, etc...) as children
 * TODO: Make the redirection to /login when click on log in.
 */
const NavBar = () => {

  /**
   * Main context.
   */
  const { addNotice } = useContext(AppContext);

  /**
   * If TRUE, the menu is open.
   */
  const [open, setOpen] = useState(false);

  /**
     * Toogle menu by clicking on the burger button.
     */
  const toggleMenu = () => {
    setOpen(!open);
  }

  /**
 * The full dept
 */
  const [dept, setDept] = useState();

  /*
   *Log Out User
   */
  const logOutUser = ({ history }: any) => {



    try {
      console.log("we log out")
      logOut()
      addNotice('You are disconnected!', 'is-warning')

    }
    catch (error) {
      addNotice(error.message, 'is-danger')
    }
  }




  /**
   * Logs Actions
   */
  const LogAction = (e: any) => {
    // const [userLog, setUserLog] = useState(false);



    // const logInUser = () => {
    //   setUserLog(true)
    //   //Have to redirect on /login . Don't know what's the best way..
    // }

    return (
      <>
        <div className="navbar-item">
          <Link to={`/login`} className="buttons" >
            <div className="button is-light" onClick={logOutUser} >
              <span className="icon">
                <FontAwesomeIcon icon={faUserAstronaut} />
              </span>
              <span>Log Out</span>
            </div>
          </Link>
        </div>
      </>
    )
  }

  useEffect(() => {
    // The full dept.
    firebase
      .firestore()
      .collection(`history`)
      .where('currentDept', ">=", 0)
      .onSnapshot(function (querySnapshot) {
        var deptData = 0;
        querySnapshot.forEach(function (doc) {
          deptData += doc.data().currentDept;
        });
        setDept(deptData)
      })
  }, []);

  return (
    <>
      <nav className="navbar is-fixed-top" role="navigation" aria-label="main navigation">
        <div className="navbar-brand home ">
          <a role="button" className={`navbar-burger home burger ${open ? 'is-active' : ''}`} aria-label="menu" aria-expanded={open} onClick={toggleMenu}>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
          <div className="navbar-item">
            <figure className="image ">
              <img src={logo} />
            </figure>
          </div>
        </div>
        <div className={`navbar-menu ${open ? 'is-active' : ''}`}>
          <div className="navbar-start">
            <div className="navbar-item">
              <Link to={`/historique/liste-dettes`} className="buttons" >
                <div className="button is-light">
                  <span className="icon">
                    <FontAwesomeIcon icon={faShieldAlt} />
                  </span>
                  <span>Dette générale  {dept}€</span>
                </div>
              </Link>
            </div>
            <LogAction />
          </div>
        </div>
      </nav>
    </>
  );
}

export default NavBar;
