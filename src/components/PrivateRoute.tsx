import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getCurrentUser } from '../api/Auth';

// Private route.
// The user needs to be authenticated to access this resource.
const PrivateRoute = ({ component: Component, ...rest }: any) => (
  <Route {...rest} render={(props) => (
    getCurrentUser()
      ? <Component {...props} />
      : <Redirect to="/login" />
  )} />
)

export default PrivateRoute;
