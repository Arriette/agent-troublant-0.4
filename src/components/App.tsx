import React, { useState } from 'react';

// App
import './App.scss';
import AppContext from './AppContext';
import AppLoader from './AppLoader';

// Router
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';

// Components
import Footer from './Footer';
import { NoticeInterface, NoticeLevel } from './Notice';

// Views
import Home from '../views/Home';
import Login from '../views/Login';
import ProfileEdit from '../views/ProfileEdit';
import ProfileView from '../views/ProfileView';
import ProfileCreate from '../views/ProfileCreate';
import ProductEdit from '../views/ProductEdit';
import ProductView from '../views/ProductView';
import ProductCreate from '../views/ProductCreate';
import HistoryDeptList from '../views/HistoryDeptList';
import HistoryDeptListSingle from '../views/HistoryDeptListSingle';
import NotFound from '../views/NotFound';
import HistoryLastRecieved from '../views/HistoryLastRecieved';
import HistoryLastSell from '../views/HistoryLastSell';
import HistoryLastRefund from '../views/HistoryLastRefund';
import HistoryReciepes from '../views/HistoryReciepe';

/**
 * Main application controller.
 */
const App = () => {

  /**
   * Notice list.
   * TODO: Move to the notice list component? (explicitely determine where to add the msg)
   */
  const [notices, setNotices] = React.useState<Array<NoticeInterface>>([]);

  const [profil, setProfile] = useState()
  const addProductToProfil = (profilAuthor:string) => {
    setProfile(profilAuthor)
  }
  /**
   * Add a new Notice.
   *
   * @param {string} body
   * @param {NoticeLevel} level
   */
  const addNotice = (body: string, level?: NoticeLevel) => {
    const notice: NoticeInterface = { body, level };
    setNotices([...notices, notice]);
  };

  /**
   * Remove a Notice from the list.
   *
   * @param {NoticeInterface} notice
   */
  const removeNotice = (notice: NoticeInterface) => {
    setNotices([
      ...notices.filter((n: NoticeInterface) => n != notice)
    ]);
  };

  // Modal component state
  const [modal, setModal] = React.useState(<></>);

  // Delete modal component
  const resetModal = () => {
    setModal(<></>)
  }

  /**
   * Provided controller.
   * Contains publicly accessible methods in the application.
   */
  const controller = {
    notices,
    addNotice,
    removeNotice,
    setModal,
    resetModal
  };
  /*
  *TODO: Shall we use name instead of id to get pretty url ? 
  */

  return (
    <AppLoader>
      <AppContext.Provider value={controller}>
        <Router>
          <Switch>
            <Route path="/login" component={Login} />
            <PrivateRoute exact path="/profil/creation" component={ProfileCreate} />
            <PrivateRoute exact path="/profil/:path/edition" component={ProfileEdit} />
            <PrivateRoute exact path="/profil/:path" component={ProfileView} action={setProfile}/>
            <PrivateRoute exact path="/fanzine/creation" component={ProductCreate} profil={profil} />
            <PrivateRoute exact path="/fanzine/:path/edition" component={ProductEdit} />
            <PrivateRoute exact path="/fanzine/:path" component={ProductView} />
            <PrivateRoute exact path="/auteurs/:path/ventes" component={HistoryDeptListSingle} />
            <PrivateRoute exact path="/historique/liste-dettes" component={HistoryDeptList} />
            <PrivateRoute exact path="/historique/livraisons" component={HistoryLastRecieved} />
            <PrivateRoute exact path="/historique/ventes" component={HistoryLastSell} />
            <PrivateRoute exact path="/historique/remboursements" component={HistoryLastRefund} />
            <PrivateRoute exact path="/historique/recettes" component={HistoryReciepes} />
            <PrivateRoute exact path="/historique" component={History} />
            <PrivateRoute exact path="/" component={Home} />
            <Route path="/introuvable" component={NotFound} />
            <Route path="*" component={NotFound} />
          </Switch>
          <Footer />
          {modal}
        </Router>
      </AppContext.Provider>
    </AppLoader>
  );
}

export default App;
