import React from 'react';
import logo from '../assets/logo.svg';
import { Link } from 'react-router-dom';

/**
 * Footer.
 * TODO: Fix content
 */
const Footer = () => (
  <section className="footer">
    <div className="content has-text-centered">
    <Link to={`/`} className="is-centered">
      <figure className="image is-48x48">
        <img src={logo} />
      </figure>
    </Link>
      <p className='subtitle dark name is-size-7'>
       Fabriquée au 7 rue Pastoret</p>
    </div>
    
  </section>
);

export default Footer;
