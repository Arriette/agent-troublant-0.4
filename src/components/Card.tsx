import React from 'react';
import Truncate from 'react-truncate';

/**
 * Comment récupérer proprement tous les élements (products, persons) et les rendre selon les dates de création?
 * Peut-on imaginer garder le même component card pour les lastActivities, lastlog etc ... ?
 * J'imagine une cloud function qui récupère tout les éléments, les trie par date de créa. A l'affichage il ne resors que les 10 premiers.
 *
 * Le plus important actuellement est de définir qu'est ce qui va être affiché suivant les résultats de recherche
 * Soit toutes les cards sont identitiques (titre/description), soit il faut faire un truc un peu plus complexe
 */


const ProfileCard = ({ data }: any) => {
  const { name, description, profile_path, image } = data;
  return (
    <div className="box profile">
      <a href={`/profil/${profile_path}`}>
        <article className="media">
          <div className="media-content">
            <div className="content">
              <h1 className="title">{name.toUpperCase()}</h1>
              <Truncate lines={2} ellipsis={<span>...</span>}>
                {description}
              </Truncate>
            </div>
          </div>
        </article>
      </a>
    </div>
  )
}

const ProductCard = ({ data }: any) => {
  const { name, product_path, stock, author, price, image } = data;
  return (
    <div className="box product">
      <a href={`/fanzine/${product_path}`}>
        <article className="media">
          <div className="media-left">
            <figure className="image  ">
              <img src={image ? image : "http://placekitten.com/640/260"} alt={name ? name : "avatar"} />
            </figure>
          </div>
          <div className="media-content">
            <div className="content">
                <h1 className="title">{name.toUpperCase()}</h1>
                <small>{(price == 0) ? "Prix libre" : price + "€"}</small>
                <br />
                <small>{stock} pce(s)  </small>
                <br />
                <p>Par <strong>{author}</strong></p>
            </div>
          </div>
        </article>

      </a>
    </div>
  );
}

const DeptListCard = ({ data }: any) => {
  const { currentDept, fullSoldSinceBegining, name, path } = data;
  return (
    <div className="box product">
      <a href={`/profil/${path}`}>
        <article className="media">
          <div className="media-content">
            <div className="content">
              <h1 className="title">{name.toUpperCase()}</h1>
              <p>Dette courante commission exclue  : <strong className="title">{currentDept.toFixed(2)}€</strong></p>
              <p>Argent récupéré depuis le début : <span className="title">{fullSoldSinceBegining.toFixed(2)}€ </span></p>
            </div>
          </div>
        </article>
      </a>
    </div>
  );
}


const Card = ({ data }: any) => {
  if (data.type == 'profile') {
    return <ProfileCard data={data} />
  }
  else if (data.type == 'product') {
    return <ProductCard data={data} />
  }
  else if (data.currentDept) {
    return <DeptListCard data={data} />
  }
  return <></>

}

/**
 * Renders a list of cards.
 * TODO: Check for each item type
 */
export const CardList = ({ items = [] }: { items?: Array<any> }) => {
  return (
    <>
      {items.map((item: {}, k: number) => {
        return typeof item !== "undefined" ? (
          <Card data={item} key={k} />
        ) : null
      })}
    </>
  )
}



// TODO: Remove
export default CardList;
