import firebase from "firebase/app";

/**
 * History
 */

//===> For HistoryDeptlist
/**
 * Load the logs of the product.
 * @param id
 */
export async function loadHistoryDeptLogs() {
  return await firebase
    .firestore()
    .collection(`history`)
    .where("currentDept", ">=", 0)
    .orderBy("currentDept", "desc")
    .get();
}

/**
 * Get profile with a not empty wallet
 */
export async function loadTheFullDept() {
  return await firebase
    .firestore()
    .collection(`history`)
    .where("currentDept", ">=", 0)
    .get();
}

//===> For HistoryLastRecieve
/**
 * Get recieve logs.
 */
export async function loadRecieveLog() {
  return await firebase
    .firestore()
    .collection(`logs`)
    .where("typeLog", "==", "recieve")
    .orderBy("timestamp", "desc")
    .get();
}

//===> For HistoryLastRefund
/**
 * Get refund logs.
 */
export async function loadRefundLog() {
  return await firebase
    .firestore()
    .collection(`logs`)
    .where("typeLog", "==", "refund")
    .orderBy("timestamp", "desc")
    .get();
}

//===> For HistoryLastSell
/**
 * Get sell logs.
 */
export async function loadSellLog() {
  return await firebase
    .firestore()
    .collection(`logs`)
    .where("typeLog", "==", "sell")
    .orderBy("timestamp", "desc")
    .get();
}
