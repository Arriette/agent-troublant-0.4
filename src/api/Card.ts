import firebase from "firebase/app";

// Get home Cards
// TODO: Format cards using an appropriate interface
export function getHomeCards() {
  return firebase
      .firestore()
      .collection("cards")
      .orderBy("timestamp", "desc")
      .get()
  }

