import firebase from "firebase/app";

/**
 * TODO : Why image commented but work?
 */

/**
 * Product interface.
 */
export interface ProductInterface {
  name: string;
  description: string;
  stock: string;
  price: string;
  format: string;
  creationDate: string;
  author: string;
  path_author?: string;
  path: string;
}

/**
 * Type the Product model for Product card.
 */
export interface ProductCardInterface {
  name: string;
  price: number;
  stock: number;
  author: string;
  product_path: string;
}

/**
 * Load one product data.
 * @param id
 */
export async function loadProduct(id: string) {
  return await firebase
    .firestore()
    .collection(`products`)
    .doc(id)
    .get()
    .then(snapshot => {
      return snapshot.data();
    })
    .catch(err => {
      console.log("Error => ", err);
    });
}

/**
 * Load the logs of the product.
 * @param id
 */
export async function loadProductLogs(id: string) {
  return await firebase
    .firestore()
    .collection(`logs`)
    .where("path", "==", id)
    .orderBy("timestamp", "desc")
    .get();
}

/**
 * Delete a product.
 * @param id
 */
export async function deleteAProduct(id: string) {
  return await firebase
    .firestore()
    .collection("products")
    .doc(id)
    .delete();
}

/**
 * Update the stock of a product.
 * @param id
 */
export async function updateProductStock(id: string, newStock : Number, priceSold: Number) {
  return await firebase
    .firestore()
    .collection("products")
      .doc(id)
      .set({
        stock: newStock,
        priceSold: priceSold
      }, { merge: true })
}

/**
 * Edit one product data.
 * @param id
 */
export async function editProduct(id: string, data: []) {
  return await firebase
    .firestore()
    .collection("products")
    .doc(id)
    .set(data, { merge: true })
    .catch(err => {
      console.log("Error => ", err);
    });
}

