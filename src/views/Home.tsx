import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faUser } from '@fortawesome/free-solid-svg-icons';


// Components
import NavBar from '../components/NavBar';
import { CardList } from '../components/Card';
import { NoticeArea } from '../components/Notice';

// API
import { getHomeCards } from '../api/Card';
import { Link } from 'react-router-dom';



/**
 * Home page
 * SEE : https://github.com/upmostly/react-hooks-infinite-scroll/blob/master/src/List2.js
 * AND : https://alligator.io/react/react-infinite-scroll/
 */
const Home = () => {

  const [cards, setCards] = useState<any>([]);
  const [searchValue, setSearchValue] = useState("")


   // Update search value state function
   function searchInputChange(event) {
     setSearchValue(event.target.value.toLowerCase())
  }

    // Search function
    function searchSubmit(event) {
      setSearchValue(searchValue.toLowerCase())
    }

    let sortedCardsBySearch = cards.filter(
      (card) => {
        // If state searchValue is not null
        if (searchValue) {
          return card.name.toLowerCase().indexOf(searchValue) !== -1 ;
        }
        else {
          return card;
        }
      }
    );

    function hideUseless(){
      var useless = document.getElementsByClassName("useless")
      for (var i = 0; i < useless.length; i++) {
        useless[i].classList.add("hideOnMobile")
    }
    }

    function showUseless(){
      var useless = document.getElementsByClassName("useless")
      for (var i = 0; i < useless.length; i++) {
        useless[i].classList.remove("hideOnMobile")
    }
    }


  useEffect(() => {

    getHomeCards().then(documentSnapshots => {
      let cardsNews = documentSnapshots.docs.map(doc => doc.data());
      setCards(cardsNews)
    })

  }, []);



  return (
    

    <>
      <NavBar />
      <NoticeArea />
      <section className="section home">
        {/* <SearchBar filtering={cardsFiltered}/> */}
        <div className="search-bar">
          <h1 className="search-bar title">pokédex de fanzines : </h1>
          <input onChange={searchInputChange} placeholder="rechercher :" id="searchSuggestion" className="input" onFocus={hideUseless} onBlur={showUseless} />
        </div>
        <hr className="useless"/>
        <div className="container">
          <h2 className="subtitle dark has-text-centered useless">dernières activités : </h2>
          <div id="cardList">
            <CardList items={sortedCardsBySearch} />
            <hr />
          </div>
        </div>
      </section>
      <Link id="create-element" to="/profil/creation" >
        <span className="icon is-large">
          <FontAwesomeIcon icon={faUser} />
          <FontAwesomeIcon icon={faPlusCircle} />
        </span>
      </Link>

    </>
  );
}

export default Home;
