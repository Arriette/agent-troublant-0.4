import React, { useEffect, useState } from "react";
import firebase from "firebase";

//Dta
import CardList from "../components/Card";
import HistoryNavBar from "../components/HistoryNavBar";
import { loadSellLog } from "../api/History";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoneyBillWave } from "@fortawesome/free-solid-svg-icons";


/**
 * History of the last Recieve
 */
const HistoryLastSell = () => {
    /**
     * These are the fanzine linked to the profil.
     */
    const [sellLogs, setSellLogs] = useState([]);

    const Sell = ({ data }: any) => {
        const { unities, date, name, path, priceSold, price } = data;
        return (
            <tr>
                <td><a href={`/fanzine/${path}`}>{name}</a></td>
                <td><p className="showOnMobile">Unités : </p>{unities}</td>
                <td><p className="showOnMobile">Prix de vente : </p>{priceSold == 0 ? price : priceSold}€</td>
                <td><p className="showOnMobile">Total : </p>{unities * (priceSold == 0 ? price : priceSold)}€</td>
                <td className="scroll"><p className="showOnMobile">Le : </p>{date}</td>
            </tr>
        );
    }

    useEffect(() => {

        // Load the sell list
        var logsLoaded = loadSellLog()
        logsLoaded
            .then(function (doc) {
                var logsdata = [];
                doc.forEach(element => {
                    logsdata.push(element.data());
                });
                setSellLogs(logsdata);
            })
    }, []);

    return (
        <>
            <HistoryNavBar />
            <section className="section history">
                <div className="container">
                    <h1 className="subtitle dark name">Dernière vente <span className="icon">
            <FontAwesomeIcon icon={faMoneyBillWave} />
          </span></h1>
                    <table>
                        <tr>
                            <th className="hideOnMobile">Fanzine</th>
                            <th className="hideOnMobile">unités</th>
                            <th className="hideOnMobile">Prix/unité</th>
                            <th className="hideOnMobile">Total</th>
                            <th className="hideOnMobile">Date&Heure</th>
                        </tr>
                        <tbody>
                            {sellLogs.map((item: {}, k: number) => {
                                return typeof item !== "undefined" ? (
                                    <Sell data={item} key={k} />
                                ) : null
                            })}
                        </tbody>
                    </table>
                </div>
            </section>
        </>
    );
};

export default HistoryLastSell;

