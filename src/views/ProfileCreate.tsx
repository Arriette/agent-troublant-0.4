import React, { useContext, useState } from "react";
//When a use require it works, not with import.
const urlSlug = require("url-slug");

//data
import AppContext from "../components/AppContext";
import { ProfileInterface, saveProfile } from "../api/Profile";
import { withRouter } from "react-router";
import { NoticeArea } from "../components/Notice";

//icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faSave } from "@fortawesome/free-solid-svg-icons";


/**
 * PERSON CREATE
 */
const ProfileCreate = ({ history }: any) => {

  // Main controller.
  const { addNotice } = useContext(AppContext);

  // Data of a profile.
  const [data, setData] = useState<ProfileInterface | any>({});

  /**
   * Save the profile
   */
  function handleSave() {
    saveProfile(data)
      .then(() => {
        addNotice(`Le profil ${data.name} a été créé`, "is-success");
        history.push(`/profil/${urlSlug(data.name)}`);
      })
      .catch(err => addNotice(err.message, "is-danger"));
  }

  /**
   * HandleChange
   * When input value change, get it.
   */
  function handleChange(e: any) {

    if (e.target.value) {
      setData({
        ...data,
        [e.target.name]: e.target.value
      });
    }
  }

  return (
    <>
    <NoticeArea />
      <nav
        className="navbar navbar is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand edition">
          <div className="navbar-start">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light" onClick={() => history.goBack()}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faArrowLeft} />
                  </span>
                </a>
              </div>
            </div>
          </div>
          <h1 className="subtitle dark name">New</h1>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light is-success" onClick={handleSave}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faSave} />
                  </span>
                  <span>Enregister</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <section className="section">
        <div className="container">
          <div className="field">
            <label className="label">Nom</label>
            <div className="control">
              <input
                className="input"
                name="name"
                type="text"
                required
                onChange={handleChange}
                placeholder="Ou blaze"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Activités</label>
            <div className="control">
              <textarea
                className="textarea"
                name="description"
                onChange={handleChange}
                placeholder="Peinture, photo, linogravure.."
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Notes pour nous :</label>
            <div className="control">
              <textarea
                className="textarea"
                name="activities"
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Téléphone</label>
            <div className="control">
              <input
                className="input"
                name="phone"
                type="text"
                onChange={handleChange}
                placeholder="06 00 00 00 00"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Email</label>
            <div className="control">
              <input
                className="input"
                name="email"
                type="text"
                onChange={handleChange}
                placeholder="jeanmich@gmail.com"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Localisation</label>
            <div className="control">
              <input
                className="input"
                name="location"
                type="text"
                onChange={handleChange}
                placeholder="Mars"
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default withRouter(ProfileCreate);
