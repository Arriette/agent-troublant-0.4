import React, { useEffect, useState } from "react";

//Component
import HistoryNavBar from "../components/HistoryNavBar";
import firebase from "firebase";

/**
 * Historyreciepe
 */
const HistoryReciepes = () => {

  /**
  * These are log of the commission recieve.
  */
  const [receiepeList, setReceiepeList] = useState([]);

  /**
  * Reciepes the last months.
  */
  const [monthlyReciepe, setMonthlyReciepe] = useState<Array<number>>([]);


  const months = [
    "Janvier",
    "Fevrier",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Aout",
    "Septembre",
    "Octobre",
    "Novembre",
    "Decembre"
  ]

  const date = new Date;

  const Reciepe = ({ data }: any) => {
    const { product, author, date, reciepe } = data;
    return (
      <tr>
        <td>{product}</td>
        <td><p className="showOnMobile">Auteur : </p>{author}</td>
        <td><p className="showOnMobile">Recette : </p>{reciepe.toFixed(2)}€</td>
        <td><p className="showOnMobile">Le : </p>{date}</td>
      </tr>
    );
  }


  useEffect(() => {
    let date = new Date;
    let threeLastMonths : Array<number> = []
    for (let key = 0; key <= 2; key++) {
    date.setMonth(date.getMonth() - key);
    let firebaseYearDate = date.getFullYear(); 
    firebase
      .firestore()
      .collection(`reciepes`)
      .where(`month`, `==`, date.getMonth() - 1)
      .where(`year`, `==`, firebaseYearDate)
      .onSnapshot(function (querySnapshot) {
        var reciepeData = 0;
        querySnapshot.forEach(function (doc) {
          reciepeData += doc.data().reciepe;
        });
        threeLastMonths.push(parseFloat(reciepeData.toFixed(2)));
      }); 
    }
    setMonthlyReciepe(threeLastMonths)

    // Load all ce reciepe log
    firebase
      .firestore()
      .collection(`reciepes`)
      .orderBy("timestamp", "desc")
      .get()
      .then(function (doc) {
        var logsdata = [];
        doc.forEach(element => {
          logsdata.push(element.data());
        });
        setReceiepeList(logsdata);
      })



  }, []);

  return (
    <>
      <HistoryNavBar />
      <section className="section history">
        <div className="container">
          <h1 className="subtitle dark name has-text-centered">Recette en commission</h1>
          <nav className="level box">
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">{months[date.getMonth() - 2]} </p>
                <p className="title">{monthlyReciepe[2]}€</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">{months[date.getMonth() - 1]} </p>
                <p className="title">{monthlyReciepe[1]}€</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">{months[date.getMonth() ]} </p>
                <p className="title">{monthlyReciepe[0]}€</p>
              </div>
            </div>
          </nav>
          <table>
            <tr>
              <th className="hideOnMobile">Fanzine : </th>
              <th className="hideOnMobile">Auteur : </th>
              <th className="hideOnMobile">Recette : </th>
              <th className="hideOnMobile">Date et heure : </th>
            </tr>
            <tbody>
              {receiepeList.map((item: {}, k: number) => {
                return typeof item !== "undefined" ? (
                  <Reciepe data={item} key={k} />
                ) : null
              })}
            </tbody>
          </table>
        </div>
      </section>
    </>
  );
};

export default HistoryReciepes;