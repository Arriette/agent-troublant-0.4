import React, { useEffect, useState, useContext } from "react";
import { NoticeArea } from "../components/Notice";

//data
import { ProfileInterface, editProfile, loadProfile } from "../api/Profile";
import AppContext from "../components/AppContext";

//icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faSave } from "@fortawesome/free-solid-svg-icons";


/**
 * PERSON EDIT
 */
const ProfileEdit = ({ match, history }: any) => {
  // Main controller.
  const { addNotice } = useContext(AppContext);

  ///Retrieve the id in path.
  const path = match.params.path;

  //Init the state
  const [data, setData] = useState<ProfileInterface | any>({});



  /**
   * SAVE MODIFICATION
   */
  function handleSave() {
    editProfile(path, data)
      .then(() => {
        addNotice(`Le profil ${data.name} a été mis à jour.`, "is-success");
        history.push(`/`);
      })
      .catch(err => addNotice(err.message, "is-danger"));
  }


  // When the values of the form changes
  function handleChange(e: any) {

    setData({
      ...data,
      [e.target.name]: e.target.value
    });
  }

  // On component load, load its data from firestore.
  useEffect(() => {
    // Scroll top on load
    window.scrollTo(0, 0)

    //Load the profile if exist
    var dataLoaded = loadProfile(path)
    dataLoaded.then((data) => {
      if (data) {
        setData(data);
      } else {
        addNotice(`Le profil que vous cherchez n'existe pas.`, "is-danger");
        history.push(`/introuvable`);
      }
    })
  }, []);

  return (
    <>
      <NoticeArea />
      <nav
        className="navbar navbar is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand edition">
          <div className="navbar-start">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light" onClick={() => history.goBack()}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faArrowLeft} />
                  </span>
                </a>
              </div>
            </div>
          </div>
          <h1 className="subtitle dark name">Edition</h1>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light is-success" onClick={handleSave}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faSave} />
                  </span>
                  <span>Enregister</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <section className="section">
        <div className="container">
          <div className="field">
            <label className="label">Nom</label>
            <div className="control">
              <input
                className="input"
                name="name"
                type="text"
                value={data.name}
                // required
                onChange={handleChange}
                disabled
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Activities</label>
            <div className="control">
              <textarea
                className="textarea"
                name="description"
                value={data.description}
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Notes pour nous :</label>
            <div className="control">
              <textarea
                className="textarea"
                name="activities"
                value={data.activities}
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Téléphone</label>
            <div className="control">
              <input
                className="input"
                name="phone"
                type="text"
                value={data.phone}
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Email</label>
            <div className="control">
              <input
                className="input"
                name="email"
                type="text"
                value={data.email}
                required
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Localisation</label>
            <div className="control">
              <input
                className="input"
                name="location"
                type="text"
                value={data.location}
                required
                onChange={handleChange}
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ProfileEdit;
