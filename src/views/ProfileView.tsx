import React, { useContext, useEffect, useState } from "react";
import { Link, withRouter } from "react-router-dom";

//data
import { ProfileInterface, deleteAProfile, moneyGivenBack, loadProfile, loadProfileCreation } from "../api/Profile";
import YesNoModal from "../components/modals/YesNoModal";
import AppContext from "../components/AppContext";
import { NoticeArea } from "../components/Notice";
import { CardList } from "../components/Card";

//icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faArrowLeft, faTrash, faHandHoldingUsd, faBook, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import logo from '../assets/logo.svg';

/**
 * PERSON VIEW
 *
 * The ID of the document is in the URL.
 * Renders certain fields in a list view.
 * Navigation contains "Previous" and "Edit" buttons.
 */
const ProfileView = ({ match, history, }: any, action: string) => {

  const newTo = {
    pathname: "/fanzine/creation",
    param1: match.params.path
  };
  // Main controller.
  const { addNotice, setModal, resetModal } = useContext(AppContext);

  //Retrieve the id in path.
  const path = match.params.path;

  //Init the state
  const [data, setData] = useState<ProfileInterface | any>({});

  /**
   * These are the fanzine linked to the profil.
   */
  const [cardsProducts, setCardsProduct] = useState([]);

  /**
   * Toogle menu by clicking on the burger button.
   */
  const toggleMenu = () => {
    setOpen(!open);
  };

  /**
   * If TRUE, the menu is open.
   */
  const [open, setOpen] = useState(false);

  /**
   * DELETION.
   */

  /**
   *Open a modal to confirm deletion
   */
  const deleteProfileConfirm = () => {
    setModal(
      <YesNoModal
        title={`Supprimer le profil ${data.name}?`}
        cancelText="Annuler"
        confirmText="Supprimer"
        action={deleteProfile}
      />
    );
  };

  /*
   * Delete the profile.
   */
  function deleteProfile() {
    if (data.wallet != 0) {
      addNotice(
        `Vous ne pouvez pas supprimé le profil ${
        data.name
        }. Il faut d'abord lui rendre son solde de ${data.wallet}.`,
        "is-danger"
      );
    } else {
      deleteAProfile(path)
        .then(() => {
          resetModal();
        })
        .then(() => {
          addNotice(`Le profil ${data.name} a été supprimé.`, "is-success");
          history.push(`/`);
        })
        .catch(err => addNotice(err.message, "is-danger"));
    }
  }

  /**
   * GIVE MONEY BACK
   */
  /**
   * Button that appear when the is sold to give back.
   */
  const SoldButton = () => {
    if (data.wallet !== 0) {
      return (
        <a className="button is-fullwidth is-primary" onClick={giveMoneyBack}>
          <span className="icon">
            <FontAwesomeIcon icon={faHandHoldingUsd} />
          </span>
          <span>Rendre le solde</span>
        </a>
      );
    } else {
      return <></>;
    }
  };

  /*
   * Open modal the confim the action
   */
  const giveMoneyBack = () => {
    setModal(
      <YesNoModal
        // title="Rendre le solde"
        content={`Vous rendez ${data.wallet.toFixed(2)}€ à ${
          data.name
          }, On est d'accord ? L'agent a fait  ${((data.wallet * 0.3)/0.7).toFixed(2)}€ de commission. `}
        cancelText="Annuler"
        confirmText="Rendu"
        action={MoneyGiven}
      />
    );
  };

  /*
  * Process the moneyBack action
   */
  function MoneyGiven() {
    moneyGivenBack(path)
      .then(() => {
        resetModal()
        addNotice(`Solde de ${data.name} remis à zéro`, "is-success");
        location.reload()
      })
      .catch(err => {
        console.log(err)

      });
  }

  /**
   * On component mount, load current Profile.
   */
  useEffect(() => {
    // Scroll top on load
    window.scrollTo(0, 0)

    //Load the profile if exist
    var dataLoaded = loadProfile(path)
    dataLoaded.then((data) => {
      if (data) {
        setData(data);
      } else {
        addNotice(`Le profil que vous cherchez n'existe pas.`, "is-danger");
        history.push(`/introuvable`);
      }
    })
    loadProfileCreation(path).then(cards => setCardsProduct(cards))
  

  }, []);

  return (
    <>
      <NoticeArea />
      <nav
        className="navbar is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <div className="navbar-start">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light" onClick={() => history.goBack()}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faArrowLeft} />
                  </span>
                </a>
                <h1 className="subtitle dark name">Profil</h1>
              </div>
            </div>
          </div>


          <a
            role="button"
            className={`navbar-burger burger ${open ? "is-active" : ""}`}
            aria-label="menu"
            aria-expanded={open}
            onClick={toggleMenu}
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>
        <div className={`navbar-menu ${open ? "is-active" : ""}`}>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <Link
                  className="button is-light"
                  to={`/profil/${path}/edition`}
                >
                  <span className="icon">
                    <FontAwesomeIcon icon={faPen} />
                  </span>
                  <span>Editer</span>
                </Link>
              </div>
            </div>
            <div className="navbar-item">
              <div className="buttons">
                <div className="button is-light" onClick={deleteProfileConfirm}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faTrash} />
                  </span>
                  <span>Supprimer</span>
                </div>
              </div>
            </div>
            <div className="navbar-item">
              <SoldButton />
            </div>
            <div className="navbar-item">
              <Link to={`/`}>
                <figure className="image ">
                  <img src={logo} />
                </figure>
              </Link>
            </div>
          </div>
        </div>
      </nav>

      <section className="section" id="profil">
        <div className="container">
          <div className="field">
            <div className="control">
              <h1 className="is-size-1">{data.name}</h1>
            </div>
          </div>
          <div className="field">
            <label className="label">Activités</label>
            <div className="control">
              <p>{data.description}</p>
            </div>
          </div>
          <div className="field">
            <label className="label">Note pour nous : </label>
            <div className="control">
              <p>{data.activities}</p>
            </div>
          </div>
          <div className="field">
            <label className="label">Date d'ajout</label>
            <div className="control">
              <p>{data.date}</p>
            </div>
          </div>
          <div className="field">
            <label className="label">Solde commission exclue</label>
            <div className="control">
              <p>{data.wallet ? data.wallet.toFixed(2) + "€" : "0€"}</p>
              <SoldButton />
            </div>
          </div>
          <div className="field">
            <label className="label">Téléphone</label>
            <div className="control">
              <a href={"tel:" + data.phone}>{data.phone}</a>
            </div>
          </div>
          <div className="field">
            <label className="label">Email</label>
            <div className="control">
              <a href={"mailto:" + data.email}>{data.email}</a>
            </div>
          </div>
          <div className="field">
            <label className="label">Localisation</label>
            <div className="control">
              <p>{data.location}</p>
            </div>
          </div>
          <hr/>
          <label className="label">Pour ajouter un nouveau fanzine :  </label>
          <Link to={
            newTo
          }>
            <div className="button is-fullwidth is-primary" >
              <span className="icon ">
                <FontAwesomeIcon icon={faPlusCircle} />
              </span>
              <span> Nouveau fanzine</span>
            </div>
          </Link>
          <hr/>
          <div className="field">
            <div className="control">
            <label className="label">Ses fanzines : </label>
              <CardList items={cardsProducts} />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default withRouter(ProfileView);
