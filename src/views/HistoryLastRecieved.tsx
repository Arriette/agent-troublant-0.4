import React, { useEffect, useState } from "react";

//Dta
import HistoryNavBar from "../components/HistoryNavBar";
import { loadRecieveLog } from "../api/History";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTruck } from "@fortawesome/free-solid-svg-icons";


/**
 * History of the last Recieve
 */
const HistoryLastRecieved = () => {

  /**
   * These are the fanzine linked to the profil.
   */
  const [recieveLogs, setRecieveLogs] = useState([]);

  const Recieve = ({ data }: any) => {
    const { unities, date, name, path } = data;
    return (
      <tr>
        <td><a href={`/fanzine/${path}`}>{name}</a></td>
        <td><p className="showOnMobile">Unités recues : </p>{unities}</td>
        <td><p className="showOnMobile">Le : </p>{date}</td>
      </tr>
    );
  }

  useEffect(() => {

    // Get recieve logs.
    var logsLoaded = loadRecieveLog()
    logsLoaded
      .then(function (doc) {
        var logsdata = [];
        doc.forEach(element => {
          logsdata.push(element.data());
        });
        setRecieveLogs(logsdata);
      })
  }, []);

  return (
    <>
      <HistoryNavBar />
      <section className="section history">
        <div className="container">
          <h1 className="subtitle dark name">Derniers fanzines reçus <span className="icon">
            <FontAwesomeIcon icon={faTruck} />
          </span></h1>
         
          <hr />
          <table>
            <tr>
              <th className="hideOnMobile">Fanzine</th>
              <th className="hideOnMobile">Unité</th>
              <th className="hideOnMobile">Date&Heure</th>
            </tr>
            <tbody>
              {recieveLogs.map((item: {}, k: number) => {
                return typeof item !== "undefined" ? (
                  <Recieve data={item} key={k} />
                ) : null
              })}
            </tbody>
          </table>
        </div>
      </section>
    </>
  );
};

export default HistoryLastRecieved;
