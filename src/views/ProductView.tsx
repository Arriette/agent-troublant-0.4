import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";

//data
import { ProductInterface, loadProduct, loadProductLogs, deleteAProduct } from "../api/Product";
import { CardList } from "../components/Card";
import YesNoModal from "../components/modals/YesNoModal";
import InputModal from "../components/modals/InputModal";
import AppContext from "../components/AppContext";
import { NoticeArea } from "../components/Notice";

//icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPen,
  faArrowLeft,
  faTrash,
  faMoneyBillWave,
  faPlus
} from "@fortawesome/free-solid-svg-icons";
import logo from '../assets/logo.svg';

/**
 * ProductView
 * NB : Log are hidden. Maybe We should remove them here.
 */
const ProductView = ({ match, history }: any) => {
  // Main controller.oduct
  const { addNotice, setModal, resetModal } = useContext(AppContext);

  //Retrieve the id in path.
  const path = match.params.path;

  //Init the state
  const [data, setData] = useState<ProductInterface | any>({});

  //These are the log of the group.
  const [logs, setLogs] = useState([]);

  /*
   * If true the menu is open.
   */
  const [open, setOpen] = useState(false);

  /*
   *Toggle Menu by clicking on the burger.
   */
  const toggleMenu = () => {
    setOpen(!open);
  };

  /**
   * DELETION.
   */

  /**
   *Open a modal to confirm deletion
   */
  const deleteProductConfirm = () => {
    setModal(
      <YesNoModal
        title={`Supprimer le fanzine ${data.name}?`}
        cancelText="Annuler"
        confirmText="Supprimer"
        action={deleteProduct}
      />
    );
  };

  /*
   * Delete the product.
   */
  function deleteProduct() {
    if (data.stock != 0) {
      addNotice(
        `Vous ne pouvez pas supprimé le fanzine ${
        data.name
        }. Il faut justifier la disparition du solde ${data.stock}.`,
        "is-danger"
      );
    } else {
      deleteAProduct(path)
        .then(() => {
          resetModal();
        })
        .then(() => {
          addNotice(`Le fanzine ${data.name} a été supprimé.`, "is-success");
          history.push(`/`);
        })
        .catch(err => addNotice(err.message, "is-danger"));
    }
  }



  /**
   * Process a sell
   */
  function sell() {
    setModal(
      <InputModal
        title="Vente"
        content="Combien d'exemplaire vendu ?"
        cancelText="Annuler"
        confirmText="Vendre"
        unitiesFor="sell"
        path={path}
        path_author={data.path_author}
        freePrice={data.price == 0 ? true : false}
        stock={data.stock}
        name={data.name}
      />
    )
  }

  function recieveProduct() {
    setModal(
      <InputModal
        title="Combien d'exemplaire confié en plus ?"
        cancelText="Annuler"
        confirmText="Recu"
        unitiesFor="recieve"
        path={path}
        stock={data.stock}
        name={data.name}
        freePrice={false}
      />
    )
  }



  const SoldButton = () => {
    if (data.stock !== 0) {
      return (
        <div>
          <div className="button is-fullwidth is-primary" onClick={() => sell()} >
            <span className="icon ">
              <FontAwesomeIcon icon={faMoneyBillWave} />
            </span>
            <span>Faire une vente</span>
          </div>
        </div>
      );
    } else {
      return <a className="button is-fullwidth is-danger">Epuisé</a>;
    }
  };

  const Price = () => {
    if (data.price == 0) {
      return (
        <div className="field">
          <label className="label orange">Prix Libre (Sans commission)</label>
        </div>
      );
    } else {
      return (
        <div className="field">
          <label className="label">Prix commission inclue</label>
          <div className="control">
            <p>{data.price}€</p>
          </div>
        </div>
      )
    }
  }

  // On component load, load its data from firestore.
  useEffect(() => {
    //Scroll top
    window.scrollTo(0, 0)

    //Load the logs of the product
    var logsLoaded = loadProductLogs(path)
    logsLoaded
      .then(function (doc) {
        var logsdata = [];
        doc.forEach(element => {
          logsdata.push(element.data());
        });
        setLogs(logsdata);
      })

    //Load the product if exist
    try {
      var dataLoaded = loadProduct(path)
      dataLoaded.then((data) => {
        setData(data);
      })
    }
    catch{
      addNotice(`Le fanzine que vous cherchez n'existe pas.`, "is-danger");
      history.push(`/introuvable`);
    }
  }, []);

  // TODO: Deconstruct object
  return (
    <>
      <NoticeArea />
      <nav
        className="navbar navbar is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <div className="navbar-start">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light" onClick={() => history.goBack()}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faArrowLeft} />
                  </span>
                </a>
                <h1 className="subtitle dark name">Fanzine</h1>
              </div>
            </div>
          </div>

          <a
            role="button"
            className={`navbar-burger burger ${open ? "is-active" : ""}`}
            aria-label="menu"
            aria-expanded={open}
            onClick={toggleMenu}
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>
        <div className={`navbar-menu ${open ? "is-active" : ""}`}>
          <div className="navbar-end">

            <div className="navbar-item">
              <div className="buttons">
                <Link
                  className="button is-light"
                  to={`/fanzine/${path}/edition`}
                >
                  <span className="icon">
                    <FontAwesomeIcon icon={faPen} />
                  </span>
                  <span>Editer</span>
                </Link>
              </div>
            </div>
            <div className="navbar-item">
              <SoldButton />
            </div>
            <div className="navbar-item">
              <div className="buttons">
                <div className="button is-light" onClick={recieveProduct}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faPlus} />
                  </span>
                  <span>Recevoir des exemplaires</span>
                </div>
              </div>
            </div>
            <div className="navbar-item">
              <div className="buttons">
                <div className="button is-light" onClick={deleteProductConfirm}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faTrash} />
                  </span>
                  <span>Supprimer</span>
                </div>
              </div>
            </div>
            <div className="navbar-item">
              <Link to={`/`}>
                <figure className="image ">
                  <img src={logo} />
                </figure>
              </Link>
            </div>
          </div>
        </div>
      </nav>
      <section className="hero">
        <div className="container">
          <img src={data.image ? data.image : "http://placekitten.com/640/260"} alt={data.name ? data.name : "avatar"} />
        </div>
      </section>
      <section className="section view">
        <div className="container" id="page-wrap">
          <div className="field">
            <div className="control">
              <h1 className="is-size-3">{data.name}</h1>
            </div>
          </div>
          <div className="field">
            <label className="label">Par</label>
            <div className="control">
              <h1 className="is-size-4"><a href={"/profil/"+data.path_author}>{data.author}</a></h1>
            </div>
          </div>
          <div className="field">
            <label className="label">Description</label>
            <div className="control">
              <p>{data.description}</p>
            </div>
          </div>
          <div className="field">
            <label className="label">Date d'ajout</label>
            <div className="control">
              <p>{data.date}</p>
            </div>
          </div>
          <div className="numberInfoView">
            <div className="field">
              <label className="label">Format</label>
              <div className="control">
                <p>{data.format}</p>
              </div>
            </div>
            <Price />
            <div className="field">
              <label className="label">Stock</label>
              <div className="control">
                <p>{data.stock}</p>
              </div>
            </div>
          </div>
            <SoldButton />
          <div className="is-hidden">
            <CardList items={logs} />
          </div>

        </div>
      </section>
    </>
  );
};

export default ProductView;
