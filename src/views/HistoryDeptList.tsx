import React, { useEffect, useState } from "react";

//Component
import CardList from "../components/Card";
import HistoryNavBar from "../components/HistoryNavBar";
import { loadHistoryDeptLogs, loadTheFullDept } from "../api/History";
import firebase from "firebase";


/**
 * History
 */
const HistoryDeptList = () => {

  /**
  * These are the fanzine linked to the profil.
  */
  const [deptList, setDeptList] = useState([]);
  /**
   * These are the fanzine linked to the profil.
   */
  const [dept, setDept] = useState();
  /**
 * Reciepe of the current month.
 */
  const [monthlyReciepe, setMonthlyReciepe] = useState();

  const months = [
    "Janvier",
    "Fevrier",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Aout",
    "Septembre",
    "Octobre",
    "Novembre",
    "Decembre"
  ]

  const date = new Date;

  const Dept = ({ data }: any) => {
    const { currentDept, name, path, fullSoldSinceBegining } = data;
    return (
      <tr>
        <td><a href={`/profil/${path}`}>{name}</a></td>
        <td><p className="showOnMobile">Dette courante : </p>{new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(currentDept)}</td>
        <td><p className="showOnMobile">Cumulée : </p>{new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(fullSoldSinceBegining)}</td>
      </tr>
    );
  }


  useEffect(() => {
    const date = new Date;

    // Load the monthly reciepe
    firebase
      .firestore()
      .collection(`reciepes`)
      .where('month', "==", date.getMonth())
      .onSnapshot(function (querySnapshot) {
        var reciepeData = 0;
        querySnapshot.forEach(function (doc) {
          console.log(doc.data().reciepe)
          reciepeData += doc.data().reciepe;
        });
        setMonthlyReciepe(reciepeData.toFixed(2))
      })


    // Load the full dept
    firebase
      .firestore()
      .collection(`history`)
      .where('currentDept', ">=", 0)
      .onSnapshot(function (querySnapshot) {
        var deptData = 0;
        querySnapshot.forEach(function (doc) {
          deptData += doc.data().currentDept;
        });
        setDept(deptData.toFixed(2))
      })

    // Load the dept list
    var logsLoaded = loadHistoryDeptLogs()
    logsLoaded
      .then(function (doc) {
        var logsdata = [];
        doc.forEach(element => {
          logsdata.push(element.data());
        });
        setDeptList(logsdata);
      })

  }, []);



  return (
    <>
      <HistoryNavBar />
      <section className="section history">
        <div className="container">
          <nav className="level box">
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">dette artiste sans la commission</p>
                <p className="title">{dept}€</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Recette du mois de {months[date.getMonth()]} en commission</p>
                <p className="title">{monthlyReciepe}€</p>
              </div>
            </div>
          </nav>
          <p className="">Dettes des artistes par ordre croissant sans commission: </p>
          <hr />
          <table>
            <thead>
              <tr>
                <th className="hideOnMobile">Nom</th>
                <th className="hideOnMobile">Courante</th>
                <th className="hideOnMobile">Cumulée</th>
              </tr>
            </thead>
            <tbody>
              {deptList.map((item: {}, k: number) => {
                return typeof item !== "undefined" ? (
                  <Dept data={item} key={k} />
                ) : null
              })}
            </tbody>
          </table>
        </div>
      </section>
    </>
  );
};

export default HistoryDeptList;
