import React, { useEffect, useState, useContext } from "react";
import firebase from "firebase/app";
//When a use require it works, not with import.
const urlSlug = require("url-slug");

//data
import { ProductInterface, loadProduct, editProduct } from "../api/Product";
import AppContext from "../components/AppContext";
import { getHomeCards } from '../api/Card';
import { autocomplete } from "./Autocomplete"


//icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faSave } from "@fortawesome/free-solid-svg-icons";
import Upload from "../components/Upload";
import { NoticeArea } from "../components/Notice";

/**
 * PRODUCT EDIT
 */
const ProductEdit = ({ match, history }: any) => {
  // Main controller.
  const { addNotice } = useContext(AppContext);

  ///Retrieve the id in path.
  const path = match.params.path;

  //Init the state
  const [data, setData] = useState<ProductInterface | any>({});

  //Data of the authors
  const [authors, setAuthors] = useState([]);

  // Adress File
  const [fileStoragePath, setFileStoragePath] = useState("image/product/");


  /**
   * SAVE MODIFICATION
   */
  function handleSave() {
    editProduct(path, data)
      .then(() => {
        addNotice(`Le fanzine ${data.name} a été mis à jour.`, "is-success");
        history.push(`/`);
      })
      .catch(err => {
        addNotice(err.message, "is-danger")
      });
  }

  function uploadToServer(compressedFile, storagePath) {
    console.log("compressedFile", compressedFile)

    //If there is a file, get it.
    if (compressedFile) {
      var reader = new FileReader();

      reader.onloadend = function (evt) {
        var storageRef = firebase.storage().ref(`${storagePath}${urlSlug(compressedFile.name)}`);
        console.warn(compressedFile); // Watch Screenshot
        var uploadTask = storageRef.put(compressedFile);

        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
          function (snapshot) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log('Upload is running');
                break;
            }
          }, function (error) {
            switch (error.code) {
              case 'storage/unauthorized':
                // User doesn't have permission to access the object
                break;

              case 'storage/canceled':
                // User canceled the upload
                break;

              case 'storage/unknown':
                // Unknown error occurred, inspect error.serverResponse
                break;
            }
          }, function () {
            // Upload completed successfully, now we can get the download URL
            uploadTask.snapshot.ref.getDownloadURL()
              .then(function (downloadURL) {
                setData({
                  ...data,
                  image: downloadURL
                });
              })
              .catch((err) => console.log("Error", err))
          });
      }

      reader.onerror = function (e) {
        console.log("Failed file read: " + e.toString());

      };
      reader.readAsArrayBuffer(compressedFile);
    }
  }

  // When the values of the form changes
  function handleChange(e: any, author?: string, path?: string) {

    if (e.target.name && (e.target.name !== 'image')) {
      setData({
        ...data,
        [e.target.name]: e.target.value
      });
    } else if (e.target.name) {
      setData({
        ...data,
        [e.target.name]: e.target.value
      });
    }
    else {
      setData({
        ...data,
        author: author,
        path_author: path
      });
    }
  }

  // On component load, load its data from firestore.
  useEffect(() => {
    // Scroll top on load
    window.scrollTo(0, 0)

    //Load the profile if exist
    var dataLoaded = loadProduct(path)
    dataLoaded.then((data) => {
      if (data) {
        setData(data);
      } else {
        addNotice(`Le profil que vous cherchez n'existe pas.`, "is-danger");
        history.push(`/introuvable`);
      }
    })

    getHomeCards().then(cards => {
      var potentialAuthor = []
      Object.keys(cards).forEach(function (key) {
        if (cards[key].group_path || cards[key].profile_path) {
          potentialAuthor.push(cards[key])
        }
      });
      autocomplete(document.getElementById("authorSuggestion"), potentialAuthor, handleChange);
      console.log(potentialAuthor)
      setAuthors(potentialAuthor)
    })
  }, []);

  /**
   * TODO: Make the arrow a goBack button with history (harder than it seems :( ...)
   */
  return (
    <>
      <NoticeArea />
      <nav
        className="navbar navbar is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <div className="navbar-start">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light" onClick={() => history.goBack()}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faArrowLeft} />
                  </span>
                </a>
              </div>
            </div>
          </div>
          <h1 className="subtitle dark name"> Mode édition</h1>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light is-success" onClick={handleSave}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faSave} />
                  </span>
                  <span>Enregister</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <Upload action={uploadToServer} oldImage={data.image} storagePath={fileStoragePath} />
      <section className="section edition">
        <div className="container">
          <div className="field">
            <label className="label">Titre</label>
            <div className="control">
              <input
                className="input"
                name="name"
                type="text"
                value={data.name}
                onChange={handleChange}
                disabled
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Auteur</label>
            <div className="autocomplete" >
              <input id="authorSuggestion" type="text" value={data.author} disabled />
              <input id="authorSuggestionPath" type="hidden" value={data.path_author} />
            </div>
            <div className="control">
            </div>
          </div>

          <div className="field">
            <label className="label">Description</label>
            <div className="control">
              <textarea
                className="textarea"
                name="description"
                value={data.description}
                onChange={handleChange}
              />
            </div>
          </div>
          <hr />
          <p>Pour enlever le prix libre, renseignez seulement un prix, commission inclue.</p>
          <hr />
          <div className="field">
            <input id="freePrice" onChange={handleChange} type="checkbox" name="freePrice" className="switch is-large" checked={(data.price == 0) ? true : false} />
            <label htmlFor="freePrice" className="label">Prix Libre</label>
          </div>
          <div className="field" id="price">
            <label className="label">Prix</label>
            <div className="control">
              <input
                className="input"
                name="price"
                type="number"
                placeholder="Entrer une valeur pour désactiver le prix libre"
                onChange={handleChange}
                value={data.price}
                required
              />
            </div>
          </div>
          <hr />
          <div className="field">
            <label className="label">Format</label>
            <div className="control">
              <input
                className="input"
                name="format"
                type="text"
                value={data.format}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ProductEdit;
